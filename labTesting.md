# Testing and Unit Testing

##Objectives

Throughout this exercise the students will practice:

* Testing various versions of a program to validate their operation
* Unit Testing

## Concepts

As you have learned in previous labs, getting a program to compile is only a minor part of programming. It is super important to test functions in order to validate that they produce the correct results.


### Exercise 1

This exercise is an adaptation of [1]. You are provided with a program that implements several versions of five simple functions, e.g. rock-paper-scisors, sorting 3 numbers. By testing the versions, you and your partner will determine which of the versions is correctly implemented.   

This first exercise requires **NO programming**, just testing.  For this first exercise, you **do not** have to implement any unit tests.  Just use you testing ability (*by hand*) to determine which function version is the one that is correctly implemented.  

For instance, let us assume that a friend provides you with a program. He claims that his program solves the following problem:

> *"given three different integers, displays the maximum value"*.  

Let's say that the program has an interfaces as follows:

![](http://i.imgur.com/BD6SmKh.png)

**Without analysing the source code**, you could determine if the program provides valid results.  You could try the following cases:

* a = 4, b = 2, c = 1, expect result: 4
* a = 3, b = 6, c = 2, expected result: 6
* a = 1, b = 10, c = 100, expected result: 100

If any of the three cases fails to give the expected result, you friend's program is not working. On the other hand, If all three cases are successful, then the program has a high probability of being correct.

In this exercise, you will be validating various versions of the following functions:

* **3 Sorts**: a program that receives three strings and sorts them in lexicographical order. For example, given `jirafa`, `zorra`, and `coqui`. Will sort them as: `coqui`, `jirafa`, and `zorra`.

* **Dice**: when the user presses the `Roll them` button, the program will generate two random numbers between 1 and 6.  The sum of the random numbers is reported. 

* **Rock, Papers, Scissors**: The user enters the play for the two players ( 'R' for Rock, 'P' for Paper and 'S' for Scissors) and specifies the number of games needed to win the tournament. The program then reports who won and the score. The program reports if a player wins the tournament.

* **Check Writer**: The user enters a number between 0 and 999,999,999 (inclusive). The program's output is a long string version of the number. For example, if the user inputs '654123' the program will output 'six hundred fifty four thousand one hundred twenty three'.

* **Zulu time**: given the time in zulu time (Greenwich Mean Time) and the military zone that the user whishes to know, the program outputs the time at that zone. The format for the input is `####` in 23 hour format, e.g. 2212. The list of valid military zones is given in http://en.wikipedia.org/wiki/List_of_military_time_zones. Examples of correct output:
    * Given 1230 and zone A, the output would be 1330
    * Given 1230 and zone N, the output would be 1130


Step 1: 
For each of the functions described above, write down in your notebook the tests that you will perform in order to determine validity. 

Step 2: 
Download the program from $XYZ$.

Step 3:
Run the program. You will see a window similar to the following:

![](http://demo05.cloudimage.io/s/resize/300/i.imgur.com/Uxnb8jP.png)

Click the **3 Sorts** button and you will get the following:

![](http://demo05.cloudimage.io/s/resize/300/i.imgur.com/p1NLIdM.png)

The "Version Alpha" combo box indicates that you are running the first version of the 3 Sorts algorithm. Use the tests that you wrote in step 1 to validate "Version Alpha". Then, do the same for versions beta, gamma and delta.  Report which is the correct version in every method. Also, explain the tests that you performed that allowed you to determine that the other versions are incorrect. 

For example, you could organice your results into a table. 

| Num | Test | Alpha | Beta | Gamma | Delta | 
| ------------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| 1| "alce", "coyote", "zorro" | "alce", "coyote", "zorro" | .... | .... |
| 2| "alce", "zorro", "coyote" | "zorro", "alce", "coyote" | .... | .... |

### Exercise 2

Running tests "by hand" each time that you run a program gets tiresome very quickly. You and your partner have done it for a few simple functions. Imagine doing the same for full-blown program!, such as a browser or word processor.

*Unit tests* help programmers produce valid code and ease the process of debugging while avoiding the tedious task of entering tests by hand on each execution.


Step 1: 
Download the proyect from $XYZ$. The proyect contains only one source code file `main.cpp`. This file contains four functions whose results are only partially correct. Your task is to write unit tests for each them to identify their wrong results.  **You do not need to rewrite the functions to correct them**.

A unit test function `test_fact()` is provided for the function `fact`. If you invoke this function from `main` and compile and run the program. You will get the following message:

```cpp
Assertion failed: (fact(2) == 2), function test_fact, file ../UnitTests/main.cpp, line 69.
``` 

This would be enough for us to claim that function `fact` is not correctly implemented. Comment the invocation of `test_fact()` from main.

Step 2:
Write unit tests for the rest of the functions. Remember that you must call each of the unit test functions from the `main` for them to run. Copy each of the functions in the provided sheet.


### References

[1] http://nifty.stanford.edu/2005/TestMe/