#include <iostream>
#include <cassert>

using namespace std;

//
// Function fact(n): Given a positive number n will return its factorial.
//
unsigned int fact(unsigned int n) {

    if (n <= 0) return 0;

    int result = 1;

    for (unsigned int i = 1; i < n; i++)
        result = result * i;

    return result;
}


// 
// Function isALetter(c):
// Given a character c will return true if it is a letter,
// otherwise returns false.
//

bool isALetter(char c) {
    return ( c >= 'A' && c <= 'z');
}

//
// Function isValidTime(n:
// Given a time in military format, e.g. 1147, determines if it is valid. 
// Returns true if valid, false otherwise.
//

bool isValidTime(unsigned int n) {
    return ( n >= 0 && n <= 2359 );
}


//
// Function gcd(a,b):
// Given two positive integeres, determines their greatest common divisor 
// and returns it.
//

int gcd ( int a, int b ) {
    int c;

    while ( a > 1 ) {
        c = a;
        a = b % a;
        b = c;
    }
    return b;
}


//
// Function test_fact():
// This function is provided as an example unit test for the fact() function. 
//

void test_fact() {
    assert( fact(1) == 1 );
    assert( fact(2) == 2);
    assert( fact(4) == 24);
    cout << "Function fact() passed all the tests!!" << endl;
}

//
// EXERCISE 3: Write your test functions here!!!
//


int main()
{
    cout << "Go ahead and test!\n";
    test_fact();
 
    return 0;
}
