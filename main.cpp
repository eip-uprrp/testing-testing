#include <QApplication>
#include "mainwindow.h"
#include <cstdlib>
#include <ctime>
#include "functions.h"
#include <cassert>
#include <QDebug>
#include <QDesktopWidget>

void testSort() {
    QString a = "AAA" , b = "BBB" , c = "CCC";
    mySortBeta(a,b,c);
    assert( a < b && b < c);

    a = "AAA", b = "CCC", c = "BBB";
    mySortBeta(a,b,c);
    assert( a < b && b < c);

    a = "BBB", b = "AAA", c = "CCC";
    mySortBeta(a,b,c);
    assert( a < b && b < c);

    a = "BBB", b = "CCC", c = "AAA";
    mySortBeta(a,b,c);
    assert( a < b && b < c);

    a = "CCC", b = "AAA", c = "BBB";
    mySortBeta(a,b,c);
    //qDebug() << a << b << c;
    assert( a < b && b < c);

    a = "CCC", b = "BBB", c = "AAA";
    mySortBeta(a,b,c);
    assert( a < b && b < c);

    qDebug() << "Passed Test for Sort!!!!";
}

void testCheck() {

    assert( checkWAlpha(91) == QString("ninety one"));
    assert( checkWAlpha(891) == QString("eight hundred ninety one"));
    assert( checkWAlpha(100) == QString("one hundred"));
    assert( checkWAlpha(59123) == QString("fifty nine thousand one hundred twenty three"));
    assert( checkWAlpha(100001) == QString("one hundred thousand one"));
    assert( checkWAlpha(100000) == QString("one hundred thousand"));
    assert( checkWAlpha(100000000) == QString("one hundred million"));
    qDebug() << "Passed Test for Check Word!!!!";
}

int main(int argc, char *argv[])
{
//    testSort();
    initCheckWMaps(); /*** DONT FORGET ***/

//    testCheck();
//    qDebug() << checkWAlpha(91);
//    qDebug() << checkWAlpha(891);
//    qDebug() << checkWAlpha(801);


    srand(time(NULL));
    QApplication a(argc, argv);
    MainWindow w;

    QDesktopWidget desktop;

    int screenWidth, screenHeight;

    screenWidth = desktop.screenGeometry().width();
    screenHeight = desktop.screenGeometry().height();
    w.move( ( screenWidth - w.width() )/2, ( screenHeight- w.height() ) /2);
    w.show();



    return a.exec();
}
