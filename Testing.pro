#-------------------------------------------------
#
# Project created by QtCreator 2014-07-03T13:18:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Testing
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    secondwindow.cpp \
    functions.cpp

HEADERS  += mainwindow.h \
    secondwindow.h \
    functions.h

FORMS    += mainwindow.ui

RESOURCES += \
    images.qrc

DEFINES     += QT_NO_DEBUG_OUTPUT
